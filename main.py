from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/',methods=['GET', 'POST'])
@app.route('/auth',methods=['GET', 'POST'])
def auth():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if isFully(username,password)==False:
            return render_template("auth.html",er="Логин или пароль пуст")
        if Exist(username,password):
            return profile(username)
        else:
            return render_template("auth.html",er="Аккаунт с такими данными не найден")
    return render_template("auth.html")
def Exist(name,password):
    with open(r'C:\Users\neopo\PycharmProjects\pythonProject5\auth.txt', 'r') as f:
        data = f.read().splitlines()
    for line in data:
        if (line.split(' ')[0] == name and line.split(' ')[1] == password):
            return True
    return False
def isFully(name,password):
    if (len(name)==0 or len(password)==0):
        return False
    return True
@app.route('/profile')
def profile(username):
    return render_template("profile.html",name=username)


if __name__ == '__main__':
    app.run()
