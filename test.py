import unittest
from main import Exist
from main import isFully
class MyTestCase(unittest.TestCase):
    def test_exist_user(self):
        self.assertEqual(False,Exist("fdsff",'ssdfasf'))
        self.assertEqual(True,Exist('Mikhail','qwerty'))
    def test_fully_data(self):
        self.assertEqual(True,isFully("dsadsa","sadsa"))
        self.assertEqual(False,isFully("","dsadfsa"))
        self.assertEqual(False,isFully("dsadsad",''))
if __name__ == '__main__':
    unittest.main()
